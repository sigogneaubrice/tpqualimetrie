package umfds.Courriel;

import static org.junit.Assert.fail;

import java.util.stream.Stream;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import org.junit.jupiter.api.Test;

public class testCourriel{
	
	String emailC;
	String titreC;
	String mesPJ1;
	String mesNPJ;
	String pieceJ;
	Courriel correct; Courriel correct2;
	
	@BeforeEach
	public void setUp() {
		//Pour faire le test des envois corrects
		emailC = "unmailcorrect123@gmail.com";
		
		titreC = "un titre";
		mesPJ1 = "Voila un message avec piece jointe";
		mesNPJ = "Voila un message sans piece";
		pieceJ = "/chemin/vers/fichier";
		
		correct = new Courriel(emailC, titreC, mesPJ1, pieceJ);
		correct2 = new Courriel(emailC, titreC, mesNPJ, "");
	}
	
	@Test
	public void envoiCorrect() {
		correct.envoyer();
		correct2.envoyer();
	}
	
	//Test avec générateur de mail incorects par methode
	@ParameterizedTest
	@MethodSource("IncMailGenerator")
	public void testMailInc(String mailInc) {
		String titreC = "un titre";
		String mesPJ = "Voila un message avec une PJ alfebhcjak:jb kajba";
		String pieceJ = "/chemin/vers/fichier";
		Courriel c = new Courriel(mailInc, titreC, mesPJ, pieceJ);
		try {
			c.envoyer();
			fail("Exception aurait du être levée");
		}catch(IllegalArgumentException e) {
			if (e.getMessage()=="Erreur : mail non conforme") {
				assert(true);
			}
		}
	}
	
	public static Stream<String> IncMailGenerator(){
		String emailIC1 = "4nmailincorrect123@gmail.com";
		String emailIC2 = "unmailincorrect123gmail.com";
		String emailIC3 = "unmailincorrect123@gmailcom";
		String emailIC4 = "unmailincorrect123@gmail.";
		String emailIC5 = "unmailincorrect123@.com";
		return Stream.of(emailIC1, emailIC2, emailIC3, emailIC4, emailIC5 );	
	}
	
	
	//Test avec valeurs passée directement
	@ParameterizedTest
	@ValueSource(strings = {"Voila un message avec une PJ alfebhcjak:jb kajba", 
			"Voila un message avec une joint alfebhcjak:jb kajba",
			"Voila un message avec une jointe alfebhcjak:jb kajba"})
	public void testPJSansPiece(String messPj) {
		String emailC = "unmailcorrect123@gmail.com";
		String titreC = "un titre";
		Courriel c = new Courriel(emailC, titreC, messPj, "");
		try {
			c.envoyer();
			fail("Exception aurait du être levée");
		}catch(IllegalArgumentException e) {
			assert(true);
		}
	}
	
	
	@ParameterizedTest(name = "{index} => mot={0}")
	@ValueSource(strings = {"Voila un message avec une PJ alfebhcjak:jb kajba", 
			"Voila un message avec une joint alfebhcjak:jb kajba",
			"Voila un message avec une jointe alfebhcjak:jb kajba"})
	public void testPJAvecPiece(String messPj) {
		String emailC = "unmailcorrect123@gmail.com";
		String titreC = "un titre";
		Courriel c = new Courriel(emailC, titreC, messPj, "aaa");
		try {
			c.envoyer();
		}catch(IllegalArgumentException e) {
			assert(false);
		}
	}
	
	
	
	
	
	
}