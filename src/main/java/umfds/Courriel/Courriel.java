package umfds.Courriel;

public class Courriel{
	private String destinataire;
	private String titre;
	private String message;
	private String pieceJ;
	
	public Courriel(String d, String t, String m, String pj) {
		this.setDestinataire(d);
		this.setMessage(m);
		this.setPieceJ(pj);
		this.setTitre(t);
	}
	
	//Setter
	public void setDestinataire(String d) {
		this.destinataire = d;
	}
	public void setTitre(String t) {
		this.titre = t;
	}
	public void setMessage(String m) {
		this.message =m;
	}
	public void setPieceJ(String pj) {
		this.pieceJ = pj;
	}
	
	//Getter
	public String getDestinataire() {
		return this.destinataire;
	}
	
	public String getMessage() {
		return this.message;
	}
	public String getPieceJ() {
		return this.pieceJ;
	}
	public String getTitre() {
		return this.titre;
	}
	
	//Methodes
	public void envoyer() {
		//Vérification de l'email:
		if(!this.getDestinataire().matches("[a-zA-Z][a-zA-Z0-9]*@[a-zA-Z]+\\.[a-zA-Z]+")) {
			throw new IllegalArgumentException("Erreur : mail non conforme");
		}
		//Verification du titre:
		if(this.getTitre().equals("")) {
			throw new IllegalArgumentException("Erreur : titre non conforme");
		}
		//Verification de la PJ
		if(this.message.matches(".*PJ.*") || this.message.matches(".*joint.*") || this.message.matches(".*jointe.*")) {
			if(this.getPieceJ().equals("")) {
				throw new IllegalArgumentException("Erreur : piece jointe manquante");
			}
		}
	}
}
